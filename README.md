# README #

### What is this repository for? ###

* This projects goal is to use networks and various featurizations of networks to understand patient outcomes.
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Dependencies: sklearn, scipy, numpy, networkx, gensim, lifelines, others?
* Run ./src/main.py

### Who do I talk to? ###

* Repo owner or admin: sw5@bcm.edu
* Other community or team contact: lichtarg@bcm.edu