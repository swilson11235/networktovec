import sys

data=open(sys.argv[1]).readlines()

edges=''

for path in data:
    path=path.strip().split('\t')
    totallen=float(len(path)-1)
    for gene in path[1:]:
        edges+='{}\t{}\t{}\n'.format(path[0],gene,1/totallen)

OUT=open('../'+'GeneGroup'+sys.argv[1].split('/')[-1],'w')
OUT.write(edges.strip())
OUT.close()
