import sys
import numpy as np

data=open(sys.argv[1]).readlines()

header=data[1].strip().split('\t')
edges=''
for line in data[2:]:
    line=line.strip().split('\t')
    pt='-'.join(line[0].split('-')[:-1])
    for i in range(1,len(line)):
        try:
            val=float(line[i])
        except:
            continue
        if np.abs(val)>2:
            edges+='{}\t{}\t{}\n'.format(pt,header[i],val)


OUT=open('../graphs/{}.txt'.format('.'.join(sys.argv[1].split('.')[:-1])),'w')

OUT.write(edges)

OUT.close()
