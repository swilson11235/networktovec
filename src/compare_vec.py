import numpy as np
import pylab as pl
from sklearn.cluster import KMeans,SpectralClustering,DBSCAN
from lifelines import KaplanMeierFitter
from lifelines import CoxPHFitter
from sklearn.manifold import TSNE
from IPython import embed
import pandas as pd


def clustergroups(X, k=3,algo='kmeans'):
    '''Clusters the groups'''
    if algo.lower()=='kmeans':
        model = KMeans(n_clusters=k, random_state=0).fit(X)
    elif algo.lower()=='dbscan':
        model = DBSCAN().fit(X)
    elif algo.lower()=='spec':
        model =SpectralClustering().fit(X)
    else:
        print('Algorithm for clustering not found.')
        exit()
    return model.labels_


def plotdata(X,labels,title='Test',resultspath='../results/'):
    '''Try tsne. X is the '''
    #X = Math.loadtxt("mnist2500_X.txt");
    #labels = Math.loadtxt("mnist2500_labels.txt");
    #Y = tsne(X, 2, X.shape[1], 20.0)# No idea what the 20 is. Need to read up on it
    #pl.scatter(Y[:,0], Y[:,1], 20, labels)

    model = TSNE(n_components=2, random_state=0)
    np.set_printoptions(suppress=True)
    try:
        Y=model.fit_transform(X)
    except:
        print title
        embed()
        exit()
    pl.scatter(Y[:, 0], Y[:, 1], c=labels, cmap=pl.cm.Spectral)
    pl.title("t-SNE")
    pl.savefig(resultspath+'tsne_'+title+'.pdf')
    pl.close()
    return

def compare_vecs(tocompare,X):
    ''' Compares the similarities of the vectors of interest'''
    
    return

def KMP(survival_times,events,labels,title='Test',resultspath='../results/'):
    '''Plots a KaplanMeier plot using the lifelines package for the groups.'''
    #survival_times = np.array([0.,3.,4.5, 10., 1.])
    #events = np.array([False, True, True, False, True])
    survival_times=np.array(survival_times)
    events=np.array(events)
    labels=np.array(labels)
    #embed()
    kmf = KaplanMeierFitter()
    i=0
    kmf.fit(survival_times[labels==i], event_observed=events[labels==i],label=str(i)+'Num of pats: {}'.format(len(events[labels==i])))
    ax = kmf.plot()#ci_force_lines=True)
    #print set(labels)
    #embed()
    for i in range(1,len(set(labels))):
        
        if sum(events[labels==i])<10:
               continue
        try:
            kmf.fit(survival_times[labels==i], event_observed=events[labels==i],label=str(i)+' Num of pats: {}'.format(len(events[labels==i])))
        except:
            print i
            exit()
        kmf.plot(ax=ax)#,ci_force_lines=True)
    #embed()

    data=[('survival_times',survival_times),('events',events),('groups',labels)]
    data=pd.DataFrame.from_items(data)
    
    cf = CoxPHFitter()
    cf.fit(data,'survival_times', event_col='events')#, strata=['groups'])
    cf.print_summary()
    p=float(cf.summary.p)
    print 'p-val: ' + str(p)
    pl.title('p:{}'.format(p))
    pl.savefig(resultspath+title+'.pdf')
    pl.close()
