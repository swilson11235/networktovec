from preprocess import *
from compare_vec import *
import glob
from IPython import embed


def select_deceased(patientdata):
    # Select Deceased patients
    t=[]
    inds=[]
    l=[]
    for node in pts:
        try:
            if patientdata[node][0]==0:
                continue
            t.append(patientdata[node][1])
        except:
            continue
        inds.append(nodes.index(node))
        l.append(patientdata[node][2])
    return t,inds,l

if __name__=='__main__':
    ####################################
    ''' Get networks '''
    ####################################
    title='Test'
    netfls=glob.glob('../data/graphs/*.txt')
    print('Network Files Found:')
    print netfls

    ####################################
    ''' Get entity list to include'''
    ####################################
    filterset=getfilter(fl='../data/FilterSet.txt')
    
    ####################################
    '''Select files to combine'''
    ####################################
    fl_handle=combine_networks(netfls,title=title)#,filterset=filterset)
    #print fl_handle
    fl_handle=('../data/graphs/Test.edgelist', '../data/graphs/Test.nodelist')
    
    ####################################
    '''Get nodes, patients, and patientdata'''
    ####################################
    nodes=open(fl_handle[1]).read().replace('\r','\n').split('\n')
    nodes=[x.strip() for x in nodes if x!='']
    pts=[node for node in nodes if 'TCGA' in node]
    patsel=[i for i,node in enumerate(nodes) if 'TCGA' in node]
    patientdata=load_survival()
    
    ####################################
    ''' Get Feature Representations'''
    ####################################
    
    for algorithm in ['NMF','tsvd','node2vec']:
        if algorithm in ['NMF','tsvd','tsne']:
            ks=[20,40,60,80,100,150]
        elif algorithm=='node2vec':
            ks=[None]
        else:
            print('Ks need to be specified')
            exit()
        for k in ks:
            if algorithm == 'NMF':
                algo='NMF-{}'.format(k)
                print('On {}'.format(algo))
                X=nmf(fl_handle,k=k) # returns (Nodes,Features)
            elif algorithm == 'tsvd':
                algo='tsvd-{}'.format(k)
                print('On {}'.format(algo))
                X=tsvd(fl_handle,k=k)
                
            elif algorithm == 'node2vec':
                algo='node2vec'
                print('On {}'.format(algo))
                X=node2vec(fl_handle,title)
            else:
                print('?')
                embed()
                exit()
            
            ####################################
            '''Use features to make hypotheses'''
            ####################################
            kclust=3
            for clusteralgo in ['spec']:
                L=clustergroups(X[patsel],k=kclust,algo=clusteralgo)
                if 'k' in clusteralgo:
                    clusteralgo+='-{}'.format(kclust)
                ####################################
                '''Validate hypotheses'''
                ####################################
                # assign labels to patient data
                data,patientdata2=assign_labels(pts,L,patientdata)

                t,inds,l=select_deceased(patientdata2)
                #embed()
                #Compare to truth
                plotdata(X[patsel,:],L,'{}_{}'.format(clusteralgo,algo))
                #embed()
                plotdata(X[inds,:],t,title='Survival_{}'.format(algo))
                plotdata(X[inds,:],np.log10(np.multiply(t,30)),title='SurvivalDays_{}'.format(algo))
                plotdata(X[inds,:],l,title='Deceased_{}_{}'.format(clusteralgo,algo))

                # KaplanMeier plot
                #
                #try:
                #    KMP(data[1],data[0],data[2],title='{}_{}'.format(clusteralgo,algo))
                #except:
                #    pass
