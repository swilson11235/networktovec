from sklearn.decomposition import NMF
import numpy as np
from IPython import embed
from scipy.sparse import csr_matrix
from io import StringIO   # StringIO behaves like a file object
import run_node2vec
import copy
from sklearn.manifold import TSNE,MDS,spectral_embedding
from sklearn.decomposition import TruncatedSVD

#############################
'''___Network Functions___'''
#############################

def combine_networks(fls,title='Test',outpath='../data/graphs/',filterset=[]):
    '''Combine any number of networks into a metanetwork. The major hurdle is the mapping, which I will ignore as a problem for now.'''
    edges=[]
    nodes=[]
    for fl in fls:
        data=open(fl)
        for line in data:
            if line[0]=='#':
                continue
            if line[0:3]=='Ent':
                continue
            line=line.strip().split('\t')
            if float(line[2])>0:
                #embed()
                #exit()
                if filterset!=[] and line[0] not in filterset and line[1] not in filterset:
                    continue
                
                nodes+=line[0:2]
                edges.append(line[0:3])
            #embed()
            #exit()
    nodes=list(set(nodes))
    outfls=output(edges,nodes,outpath=outpath)
    return outfls

def output(edges,nodes,title='Test',outpath='../data/graphs/'):
    '''Creates the edgelist file with the correct format and the corresponding list of entities'''
    #
    OUTnodes=open(outpath+title+'.nodelist','w')
    nodes=sorted(list(nodes))
    for node in nodes:
        OUTnodes.write('{}\n'.format(node))
    OUTnodes.close()
    dic={x:i for i,x in enumerate(nodes)}
    
    OUT=open(outpath+title+'.edgelist','w')
    #OUTPUT FORMAT:int1 int2 floatconf
    for edge in edges:
        OUT.write('{}\t{}\t{}\n'.format(dic[edge[0]],dic[edge[1]],edge[2]))
    OUT.close()
    return outpath+title+'.edgelist',outpath+title+'.nodelist'

def getfilter(fl='../Data/FilterSet.txt'):
    '''Gets a list of entities to filter by'''
    try:
        ls=open(fl).read().replace('\r','\n').split('\n')
    except:
        return []
    ls=[x.strip() for x in ls]
    return ls

def read_network(network,title='Test',datapath='../data/graphs/',smooth=1):
    rows = []
    cols = []
    contents = []
    edges=open(network).readlines()
    edges=[ x.split('\t')[:3] for x in edges ]
    entities=[]
    for edge in edges:
        pos1 = int(edge[0])
        pos2 = int(edge[1])
        weight = float(edge[2])
        entities+=[pos1,pos2]
        if weight<0:
            continue
        
        rows.append(pos1)
        cols.append(pos2)
        contents.append(weight)

        rows.append(pos2)
        cols.append(pos1)
        contents.append(weight)
    entities=list(set(entities))
    #embed()
    X = csr_matrix((contents, (rows, cols)),shape=(len(entities),len(entities)))
    np.savez(datapath+'combined'+title+'edgelist', data=X.data,
             indices=X.indices,
             indptr=X.indptr,
             shape=X.shape)
    # Smooth Network
    if smooth:
        print('Smoothing network')
        X=laplaciansmoothing(X)
    return X

def laplaciansmoothing(X):
    '''Takes a symmetrical matrix and returns the symmetric normalized laplacian'''
    #embed()
    Dinv=np.power(np.sum(X,axis=0),-0.5)
    X=np.dot(np.dot(np.diag(Dinv),X),np.diag(Dinv))

    return X

####################################
'''___Create Feature Matricies___'''
####################################

def nmf(network, k=10):
    '''Returns the latent factors of the network'''
    edgels,nodels=network
    X=read_network(edgels,smooth=0)
    model = NMF(n_components=k, init='nndsvd', random_state=0)
    #model = NMF(n_components=k, init='random', random_state=0)
    model.fit(X)
    #w=model.fit_transform(M)
    return model.components_.T

def node2vec(network,title, embeddingpath='../data/emb/'):
    '''Reads in the embedding file, returns the matrix. Features are in the second / columns dimension'''
    edgels,nodels=network
    run_node2vec.run(edgels,title,outpath=embeddingpath)
    data=open(embeddingpath+title+'.emb').readlines()
    
    c=StringIO(u''.join(data[1:]))
    X=np.loadtxt(c)
    return X

def tsvd(network, k=10):#
    '''Reads in the network and conducts Truncated Single Value Decomposition to return (Nodes,Features)'''
    
    edgels,nodels=network
    X=read_network(edgels,smooth=0)
    #embed()
    model=TruncatedSVD(k)
    model.fit(X)
    return model.components_.T


def tsne(network, k=10):
    '''Reads in the network and conducts Truncated Single Value Decomposition, after SVD, it attempts tsne to return (Nodes,Features). However, this currently gets into memory problems'''
    edgels,nodels=network
    X=read_network(edgels,smooth=0)

    if X.shape[0]>10000:
        print('Error with size of data')
        exit()
    
    modelsvd=TruncatedSVD(k)
    modelsvd.fit(X)
    
    model = TSNE(n_components=k, random_state=0)
    Y=model.fit_transform(modelsvd.components_.T)

    return Y

####################################
'''       ___Load Truth___       '''
####################################

def load_survival(fl='../data/truths/HNSC.txt'):
    '''Loads survival data from cBioportal's streamlined format'''
    
    #survival_times=[]
    #events=[]
    #patients=[]
    patients={}
    lines=open(fl).read().replace('\r','\n').split('\n')
    for line in lines[1:]:
        line=line.strip().split('\t')
        if line[1]=='deceased':
            val=1
        elif line[1]=='censored':
            val=0
        else:
            print 'inside load'
            embed()
            exit()
        patients[line[0]]=[val,float(line[2])]
    return patients

def assign_labels(nodes,labels,patientdata):
    ''' Takes survival data in form of dictionary of patients with their data and assigns the labels.'''
    patientdata2=copy.deepcopy(patientdata)
    for i in range(0,len(nodes)):
        
        node=nodes[i]
        label=labels[i]
        try:
            patientdata2[node].append(label)
        except:
            continue
    
    for key in patientdata2.keys():
        if len(patientdata2[key])!=3:
            del patientdata2[key]
    data=zip(*patientdata2.values())
    return data,patientdata2
    
