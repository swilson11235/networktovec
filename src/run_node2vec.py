
import numpy as np
import networkx as nx
import node2vec
from gensim.models import Word2Vec

def read_graph(fl,weighted=1,directed=1):
	'''
	Reads the input network in networkx.
	'''
	if weighted:
		G = nx.read_edgelist(fl, nodetype=int, data=(('weight',float),), create_using=nx.DiGraph())
	else:
		G = nx.read_edgelist(fl, nodetype=int, create_using=nx.DiGraph())
		for edge in G.edges():
			G[edge[0]][edge[1]]['weight'] = 1

	if not directed:
		G = G.to_undirected()

	return G

def learn_embeddings(walks,title,outputpath='../data/emb/',dimensions=128,window_size=10,workers=4,iters=1):
	'''
	Learn embeddings by optimizing the Skipgram objective using SGD.
	'''
	walks = [map(str, walk) for walk in walks]
	model = Word2Vec(walks, size=dimensions, window=window_size, min_count=0, sg=1, workers=workers, iter=iters)
	model.save_word2vec_format(outputpath+title+'.emb')
	
	return

def run(networkfl,title,outpath='../data/emb/',directed=1,num_walks=10,walk_length=80):
	'''
	Pipeline for representational learning for all nodes in a graph.
	'''
	nx_G = read_graph(networkfl)
	G = node2vec.Graph(nx_G, directed, 1, 1)
	G.preprocess_transition_probs()
	walks = G.simulate_walks(num_walks, walk_length)
	learn_embeddings(walks,title,outpath)
